package no.noroff.nicholas.solution1;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    // Depending on abstraction not implementaiton (DIP)
    Sensor _sensor;
    boolean alarmOn = false;

    // We add a default constructor to set out default to SimpleSensor
    public Alarm() {
    }
    // Add overloaded to take in a different sensor. Now we are open for extension (OCP)
    public Alarm(Sensor _sensor) {
        this._sensor = _sensor;
    }

    public void check(){
        double psiPressureValue = _sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
