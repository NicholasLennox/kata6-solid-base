package no.noroff.nicholas.scenario2;

import org.apache.commons.text.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class UnicodeFileToHTMLTextConverter {
    private String fullFilenameWithPath;

    public UnicodeFileToHTMLTextConverter(String fullFilenameWithPath){
        this.fullFilenameWithPath = fullFilenameWithPath;
    }

    public String convertToHtml() throws IOException {
        /*
         This class should have the single responsibility of converting unicode to html
         BUT we have it with the added responsibility of reading in file information.
         This should be done somewhere else and passed back here.
        */
        try(BufferedReader reader = new BufferedReader(new FileReader(fullFilenameWithPath))){
            String line = reader.readLine();
            String html = "";
            while (line != null){
                html += StringEscapeUtils.escapeHtml4(line);
                html += "<br />";
                line = reader.readLine();
            }
            return html;
        }
    }
}
